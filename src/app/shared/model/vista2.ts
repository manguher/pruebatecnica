import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


export interface Vista2 {
    repeticiones: any[];
    suma: number;
}

const abc = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "ñ", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];


@Injectable()
export class Vista2Model {
    constructor() { }

    getDataVista2(dataVista2: any): any[] {

        let listaData: Vista2[] = [];
        let data = JSON.parse(dataVista2)

        data.forEach(element => {

            let obj: any[] = [];

            abc.forEach(item => {

                var objRepeticiones = {
                    letra: item,
                    cantidad: element.paragraph.split(item).length - 1,
                }
                obj.push(objRepeticiones)
            })

            let dataVista2: Vista2 = {
                repeticiones: obj,
                suma: this.sumaNumeros(element.paragraph)
            }
            listaData.push(dataVista2);
        });

        return listaData;
    }

    sumaNumeros(paragraph: string) {

        let arrayPalabra = paragraph.split(' ');
        let listNumeros: number[] = [];
        arrayPalabra.forEach(palabra => {
            let numero = palabra.match(/\d+/g);

            // "," entre numeros
            if (numero != null) {
                if (numero.length == 2)
                    listNumeros.push(parseInt(numero[1].substr(numero[1].length - 1)))
                else {
                    listNumeros.push(parseInt(numero[0]))
                }
            }
        });

        return listNumeros.reduce((a, b) => a + b, 0)
    }
}
