import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Vista1 } from '../model/vista1';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RestApiService {

  apiURLVista1 = 'http://patovega.com/prueba_frontend/array.php';
  apiURLVista2 = 'http://patovega.com/prueba_frontend/dict.php';

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  getDataVista1(): Observable<Vista1> {
    return this.http.get<Vista1>(this.apiURLVista1 + '/vista1')
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  getDataVista2(): Observable<any> {
    return this.http.get(this.apiURLVista2 + '/vista2')
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {

      // Error del lado del cliente
      errorMessage = error.error.message;
    } else {

      // Error del lado del Servidor
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }

    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
