import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from "../shared/services/rest-api.service"
import { Vista2Model } from "../shared/model/vista2"


@Component({
  selector: 'app-vista2',
  templateUrl: './vista2.component.html',
  styleUrls: ['./vista2.component.css']
})

export class Vista2Component implements OnInit {

  listDataService: any = [];
  listDataVista2: any = [];
  public loading: boolean = false;

  constructor(
    public vistaService: RestApiService,
    public router: Router
  ) { }

  ngOnInit() {
  }

  loadVistaData2() {
    this.loading = true;
    try {
      return this.vistaService.getDataVista2().subscribe((data: {}) => {

        this.listDataService = data;
        if (this.listDataService.success && this.listDataService.data != null) {
          let vista2Model = new Vista2Model();
          this.listDataVista2 = vista2Model.getDataVista2(this.listDataService.data)
          this.loading = false;
        }
        else {
          console.log(this.listDataService.error)
        }
      });
    } catch (error) {
      console.log(error);
      this.loading = false;
    }
  }
}
