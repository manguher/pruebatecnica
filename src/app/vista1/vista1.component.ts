import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from "../shared/services/rest-api.service"
import { Vista1, Vista1Model } from '../shared/model/vista1';
import { ignoreElements } from 'rxjs/operators';
import { stringify } from 'querystring';


@Component({
  selector: 'app-vista1',
  templateUrl: './vista1.component.html',
  styleUrls: ['./vista1.component.css']
})
export class Vista1Component implements OnInit {

  DataVista1: any = [];
  listDataService: any = [];
  listDataTexbox: any = [];
  loading: boolean = false;

  constructor(
    public vistaService: RestApiService,
    public router: Router
  ) { }

  ngOnInit() {
  }

  loadVistaData1() {

    this.loading = true;
    this.listDataTexbox = [];
    let listadoFiltrado: Vista1[] = []
    let listaRepeticiones: Vista1[] = [];

    try {
      return this.vistaService.getDataVista1().subscribe((data: {}) => {

        this.listDataService = data;

        if (this.listDataService.success && this.listDataService.data != null) {

          this.listDataService.data.reduce((contadorNumero, numero, index) => {

            listaRepeticiones.push({
              index: index,
              numero: numero,
              quantity: contadorNumero[numero] = (contadorNumero[numero] || 0) + 1,
              firstPosition: 0,
              lastposition: 0
            })

            return contadorNumero;
          }, {});

          // indices
          listaRepeticiones.forEach(element => {

            var firstPosition = listaRepeticiones.map(function (e) {
              return e.numero;
            }).indexOf(element.numero);

            var lastposition = listaRepeticiones.map(function (e) {
              return e.numero;
            }).lastIndexOf(element.numero);

            element.firstPosition = firstPosition;
            element.lastposition = lastposition;
          });

          // separa repetidos
          var repetidos = listaRepeticiones.filter(function (item, index) {
            return item.quantity > 1
          })

          // excluye repetidos
          listadoFiltrado = listaRepeticiones.filter(function (item, index) {
            return !repetidos.includes(item)
          })

          // asigna cantidad de repeticiones
          listadoFiltrado.forEach(item => {
            repetidos.forEach(element => {
              if (item.numero == element.numero)
                item.quantity = element.quantity;
            });

            // lista texbox
            this.listDataTexbox.push(item.numero)
          });
          this.DataVista1 = listadoFiltrado;


          // lista texbox order asc
          let vista2Model = new Vista1Model();
          this.listDataTexbox = vista2Model.orderAsc(this.listDataTexbox);
          this.loading = false;

        }
        else {
          console.log(this.listDataService.error)
        }
      });

    } catch (error) {
      console.log(error);
      this.loading = false;
    }
  }
}


